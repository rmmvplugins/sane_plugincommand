//==============================================================================
// Sane_PluginCommand.js
//==============================================================================

/*:
 * @plugindesc Make plugin commands sane.
 * @author Steven Rogers
 *
 * @help
 * =============================================================================
 * Introduction
 * =============================================================================
 *
 * Instead of every plugin saving a copy of Game_Interpreter.pluginCommand
 * function, doing something, then calling the original. This plugin converts
 * the plugin command system so you can subscribe to plugin command events
 * with callbacks that are only called once.
 *
 * It's better. It's faster. It's sane.
 *
 *
 * =============================================================================
 * Dependencies
 * =============================================================================
 *
 * None.
 *
 *
 *
 * =============================================================================
 * How to Use
 * =============================================================================
 *
 * Add the following somewhere in your plugin code:
 *
 *   Game_Interpreter.addPluginCommand('MyPluginCommand', function(args) {
 *     // Wow, I don't to do anything else. No regular expression, no fancy if
 *     // statements. No rewiring of internal components to make a simple system
 *     // function. It just works!
 *
 *     // Your plugin command code here.
 *
 *     // Since I don't want any other plugin commands to interfere,
 *     // I will return true and stop them:
 *     return true;
 *   });
 *
 * And you're done.
 *
 * There can only be one callback per plugin command keyword. The second
 * subscription to the same keyword will overwrite the callback function. Use
 * addPluginCommand for each of your plugin commands instead of the usual
 * all-encompassing pluginCommand function override.
 *
 * To remove a subscription:
 *
 *   Game_Interpreter.removePluginCommand('MyPluginCommand');
 *
 * You can prevent other plugins which don't use this plugin from running their
 * pluginCommand code (for this keyword) by returning true at the end of your
 * callback. You should almost always do this because plugin command keywords
 * are typically unique and so those other plugins' code is wasted CPU cycles
 * burning through precious FPS.
 */

(() => {
	// Making pluginCommand "sane" by doing what every other plugin does.
	// The irony is not lost on me.
	const Game_Interpreter__pluginCommand =
		Game_Interpreter.prototype.pluginCommand;

	// -----------------------------------------------------------------------------

	const commands = {};
	const noop = function() {};

	// -----------------------------------------------------------------------------

	Game_Interpreter.addPluginCommand = function(commandName, callback) {
		commands[commandName] = callback;
	};

	Game_Interpreter.removePluginCommand = function(commandName) {
		commands[commandName] = undefined;
	};

	Game_Interpreter.prototype.pluginCommand = function(commandName, args) {
		const callback = commands[commandName] || noop;
		if (callback(args, commandName)) {
			return;
		}

		Game_Interpreter__pluginCommand.call(this, commandName, args);
	};
})();
